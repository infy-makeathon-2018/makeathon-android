package com.infosys.makeathon.kisanyojna.data

import androidx.room.TypeConverter
import com.infosys.makeathon.kisanyojna.data.entity.Coordinate



class Converters {
    @TypeConverter
    fun fromCoordString(value: String?): Coordinate? {
        if (value == null) {
            return null
        }
        return Coordinate(value)
    }

    @TypeConverter
    fun coordToCoordString(coordinate: Coordinate?): String? {
        return coordinate?.toString()
    }

}