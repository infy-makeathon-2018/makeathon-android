package com.infosys.makeathon.kisanyojna.repository

import androidx.lifecycle.LiveData
import com.infosys.makeathon.kisanyojna.Webservice
import com.infosys.makeathon.kisanyojna.data.entity.Weather
import androidx.lifecycle.MutableLiveData
import com.infosys.makeathon.kisanyojna.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class WeatherRepository {
    private var webservice: Webservice

    init {
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.WEATHER_BASE_URL)
                .build()
        webservice = retrofit.create(Webservice::class.java)
    }

    fun getWeather(lat:Double, lon: Double): LiveData<Weather> {
        val data = MutableLiveData<Weather>()
        webservice.getCurrentWeather(lat, lon).enqueue(object : Callback<Weather> {
            override fun onResponse(call: Call<Weather>, response: Response<Weather>) {
                // error case is left out for brevity
                data.value = response.body()
            }

            override fun onFailure(call: Call<Weather>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })
        return data

    }
}