package com.infosys.makeathon.kisanyojna.ui.home.result

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.infosys.makeathon.kisanyojna.R
import com.infosys.makeathon.kisanyojna.adapter.DiseaseAdapter
import com.infosys.makeathon.kisanyojna.custom.ItemOffsetDecoration
import com.infosys.makeathon.kisanyojna.ui.result.PredictionResultViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.prediction_result_fragment.*

class PredictionResultFragment : Fragment() {

    private lateinit var viewModel: PredictionResultViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.prediction_result_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PredictionResultViewModel::class.java)
        // TODO: Use the ViewModel

        val viewManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val diseaseAdapter = DiseaseAdapter(arrayOf("Hello World", "Second Item", "Third Item"))

        val recyclerView = diseaseList.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = diseaseAdapter
        }
        recyclerView.addItemDecoration(ItemOffsetDecoration(32))
        Picasso.get().load("https://picsum.photos/400?random").into(imageView)
    }

}
