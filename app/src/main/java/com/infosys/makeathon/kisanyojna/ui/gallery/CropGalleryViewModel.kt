package com.infosys.makeathon.kisanyojna.ui.gallery

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel;
import com.infosys.makeathon.kisanyojna.data.entity.Crop
import com.infosys.makeathon.kisanyojna.repository.CropRepository

class CropGalleryViewModel : ViewModel() {
    private lateinit var cropRepository: CropRepository
    private lateinit var mAllCrops: LiveData<List<Crop>>

    fun init(context: Context) {
        cropRepository = CropRepository(context)
        mAllCrops = cropRepository.getAllWeather()
    }

    fun getAllCrops(): LiveData<List<Crop>> {
        return mAllCrops
    }
}
