package com.infosys.makeathon.kisanyojna.ui.disease

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.infosys.makeathon.kisanyojna.R
import iammert.com.expandablelib.ExpandableLayout
import kotlinx.android.synthetic.main.disease_description_child.*
import kotlinx.android.synthetic.main.disease_detail_fragment.*
import kotlinx.android.synthetic.main.disease_detail_group_parent.*


class DiseaseDetailFragment : Fragment() {

    companion object {
        fun newInstance() = DiseaseDetailFragment()
    }

    private lateinit var viewModel: DiseaseDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.disease_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DiseaseDetailViewModel::class.java)

        description_el.setRenderer(object : ExpandableLayout.Renderer<Void, Void> {
            override fun renderParent(view: View, model: Void, isExpanded: Boolean, parentPosition: Int) {
                textView8.text = "Summary"
            }

            override fun renderChild(view: View, model: Void, parentPosition: Int, childPosition: Int) {
                textView9.text = "Lorem Ipsum"
            }
        })
    }

}
