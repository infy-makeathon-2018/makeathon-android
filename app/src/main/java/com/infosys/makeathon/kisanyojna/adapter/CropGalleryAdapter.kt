package com.infosys.makeathon.kisanyojna.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.infosys.makeathon.kisanyojna.R
import com.infosys.makeathon.kisanyojna.data.entity.Crop
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.crop_gallery_item.view.*
import java.io.File

class CropGalleryAdapter(private var myDataSet: List<Crop>) :
        RecyclerView.Adapter<CropGalleryAdapter.MyViewHolder>() {

    companion object {
        private const val TAG = "CropGalleryAdapter"
    }

    fun setData(data: List<Crop>) {
        this.myDataSet = data
        notifyDataSetChanged()
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CropGalleryAdapter.MyViewHolder {
        // create a new view
        val cardView = LayoutInflater.from(parent.context)
                .inflate(R.layout.crop_gallery_item, parent, false) as CardView

        return MyViewHolder(cardView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Log.d(TAG, "image " + myDataSet[position])
        Picasso.get().load(File(myDataSet[position].imageAbsolutePath)).into(holder.cardView.imageView)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataSet.size
}