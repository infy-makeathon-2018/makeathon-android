package com.infosys.makeathon.kisanyojna

import com.infosys.makeathon.kisanyojna.data.entity.Weather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Webservice {

    @GET("/data/2.5/weather?appid=${Constants.WEATHER_API_KEY}&units=metric")
    fun getCurrentWeather(@Query("lat") lat: Double, @Query("lon") lon: Double): Call<Weather>
}