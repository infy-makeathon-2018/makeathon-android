package com.infosys.makeathon.kisanyojna.ui.gallery

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Environment
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.infosys.makeathon.kisanyojna.R
import com.infosys.makeathon.kisanyojna.adapter.CropGalleryAdapter
import kotlinx.android.synthetic.main.crop_gallery_fragment.*

class CropGalleryFragment : Fragment() {

    private lateinit var viewModel: CropGalleryViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.crop_gallery_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CropGalleryViewModel::class.java)
        viewModel.init(context!!)

        val viewManager = LinearLayoutManager(context)
//        val storageDir = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
//        val files = storageDir.listFiles().map {
//            it.absolutePath
//        }

        val cropGalleryAdapter = CropGalleryAdapter(emptyList())
        viewModel.getAllCrops().observe(this, Observer {
            cropGalleryAdapter.setData(it)
        })

        cropGalleryList.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = cropGalleryAdapter
        }

    }

}
