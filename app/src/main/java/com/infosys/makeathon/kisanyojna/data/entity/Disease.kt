package com.infosys.makeathon.kisanyojna.data.entity

import androidx.room.Entity

@Entity
data class Disease(
        val name: String,
        val description: String,
        val symptoms: List<String>,
        val precautions: List<String>,
        val cure: List<String>
)