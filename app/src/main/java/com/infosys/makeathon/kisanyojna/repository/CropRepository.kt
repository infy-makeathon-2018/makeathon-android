package com.infosys.makeathon.kisanyojna.repository

import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.infosys.makeathon.kisanyojna.data.AppDatabase
import com.infosys.makeathon.kisanyojna.data.dao.CropDao
import com.infosys.makeathon.kisanyojna.data.entity.Crop

class CropRepository(
        mContext: Context
) {

    private var mCropDao: CropDao
    private var mAllCrops: LiveData<List<Crop>>

    init {
        val db = AppDatabase.getInstance(mContext)
        mCropDao = db!!.cropDao()
        mAllCrops = mCropDao.getCrops()
    }

    fun insert(crop: Crop) {
        InsertAsyncTask(mCropDao).execute(crop)
    }

    fun getAllWeather(): LiveData<List<Crop>> {
        return mAllCrops
    }

    class InsertAsyncTask(private val mAsyncTaskDao: CropDao?): AsyncTask<Crop, Void, Void>() {

        override fun doInBackground(vararg params: Crop?): Void? {
            mAsyncTaskDao?.insert(params[0])
            return null
        }
    }

}