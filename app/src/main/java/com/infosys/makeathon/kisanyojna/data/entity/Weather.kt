package com.infosys.makeathon.kisanyojna.data.entity

import com.google.gson.annotations.SerializedName

data class Weather(
        val name: String,
        val coord: Coordinate,
        @SerializedName("weather") val weatherMetas: List<WeatherMeta>,
        val main: Main,
        val wind: Wind
)

data class Coordinate(
        val lat: Double,
        val lon: Double
){
    constructor(coordStr: String) : this(coordStr.split(",")[0].toDouble(), coordStr.split(",")[1].toDouble())

    override fun toString(): String {
        return "$lat,$lon"
    }
}

data class Main(
        val temp: Double,
        val pressure: Double,
        val humidity: Int
)

data class WeatherMeta(
        val icon: String
)

data class Wind(
        val speed: Double
)