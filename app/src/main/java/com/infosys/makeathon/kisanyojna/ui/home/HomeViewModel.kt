package com.infosys.makeathon.kisanyojna.ui.home

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.infosys.makeathon.kisanyojna.data.entity.Coordinate
import com.infosys.makeathon.kisanyojna.data.entity.Crop
import com.infosys.makeathon.kisanyojna.data.entity.Weather
import com.infosys.makeathon.kisanyojna.repository.CropRepository
import com.infosys.makeathon.kisanyojna.repository.WeatherRepository

class HomeViewModel : ViewModel() {
    private lateinit var coordinate: Coordinate
    private var weather: LiveData<Weather>? = null
    private var weatherRepository = WeatherRepository()
    private lateinit var cropRepository: CropRepository

    fun init(context: Context, lat: Double, lon: Double) {
        if (this.weather != null) {
            return
        }
        this.coordinate = Coordinate(lat, lon)
        this.cropRepository = CropRepository(context)
        this.weather = weatherRepository.getWeather(lat, lon)
    }

    fun insert(crop: Crop) = cropRepository.insert(crop)

    fun getWeather(): LiveData<Weather>? {
        return weather
    }

    fun getCoordinate(): Coordinate {
        return coordinate
    }
}
