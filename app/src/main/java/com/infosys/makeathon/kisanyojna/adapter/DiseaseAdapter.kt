package com.infosys.makeathon.kisanyojna.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.infosys.makeathon.kisanyojna.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.disease_item.view.*

class DiseaseAdapter(private val myDataSet: Array<String>) :
        RecyclerView.Adapter<DiseaseAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): DiseaseAdapter.MyViewHolder {
        // create a new view
        val cardView = LayoutInflater.from(parent.context)
                .inflate(R.layout.disease_item, parent, false) as CardView

        return MyViewHolder(cardView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.cardView.textView2.text = myDataSet[position]
        holder.cardView.button.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_predictionResult_to_diseaseDetailFragment)
        }
        Picasso.get().load("https://picsum.photos/400?random").into(holder.cardView.imageView2)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataSet.size
}