package com.infosys.makeathon.kisanyojna.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.infosys.makeathon.kisanyojna.data.entity.Crop

@Dao
interface CropDao {

    @Insert
    fun insert(crop: Crop?)

    @Query("SELECT * from Crop")
    fun getCrops(): LiveData<List<Crop>>

}