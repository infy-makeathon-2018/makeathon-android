package com.infosys.makeathon.kisanyojna.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.infosys.makeathon.kisanyojna.data.dao.CropDao
import com.infosys.makeathon.kisanyojna.data.entity.Crop

@Database(entities = [Crop::class], version = 1,  exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun cropDao(): CropDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context) : AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java, "crop.db")
                            .build()
                }
            }

            return INSTANCE
        }
    }
}