package com.infosys.makeathon.kisanyojna.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Crop(
        @PrimaryKey val imageTakenOn: String,
        val imageAbsolutePath: String,
        val diseaseName: String?,
        val imageTakenAt: Coordinate
)