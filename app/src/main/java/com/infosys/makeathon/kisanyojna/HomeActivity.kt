package com.infosys.makeathon.kisanyojna

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.infosys.makeathon.kisanyojna.ui.home.HomeFragment


class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
    }

}
