package com.infosys.makeathon.kisanyojna.ui.home

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.infosys.makeathon.kisanyojna.R
import com.infosys.makeathon.kisanyojna.data.entity.Coordinate
import com.infosys.makeathon.kisanyojna.data.entity.Crop
import com.infosys.makeathon.kisanyojna.databinding.HomeFragmentBinding
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.home_fragment.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class HomeFragment : Fragment() {

    private lateinit var mCurrentPhotoPath: String
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var binding: HomeFragmentBinding

    companion object {
        const val TAG = "HomeFragment"
        const val REQUEST_IMAGE_CAPTURE = 1
        const val REQUEST_LOCATION_PERMISSION = 2
    }

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.home_fragment, container, false) as HomeFragmentBinding
        val view = binding.root
        binding.setLifecycleOwner(this)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        updateWeather()
        getCropImages()
        openCameraButton.setOnClickListener { dispatchTakePictureIntent() }
        textView6.setOnClickListener { openGallery() }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(REQUEST_LOCATION_PERMISSION)
    fun updateWeather() {
        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity as Activity)
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                viewModel.init(context!!, location.latitude, location.longitude)
                binding.viewmodel = viewModel
            }
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.location_permission_rationale),
                    REQUEST_LOCATION_PERMISSION, Manifest.permission.ACCESS_COARSE_LOCATION)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val file = File(mCurrentPhotoPath)
            Toast.makeText(context!!, "File exists " + (file.exists()), Toast.LENGTH_LONG).show()
            Navigation.findNavController(view!!).navigate(R.id.action_homeFragment_to_predictionResult)
            viewModel.insert(Crop(
                    SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date()),
                    mCurrentPhotoPath,
                    null,
                    viewModel.getCoordinate()))
        }
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                Toast.makeText(context, R.string.generic_error_message, Toast.LENGTH_SHORT).show()
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFile.getUri())
                takePictureIntent.putExtra(MediaStore.EXTRA_SHOW_ACTION_ICONS, false)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun File.getUri() =
            FileProvider.getUriForFile(context!!,
                    "com.infosys.makeathon.kisanyojna.fileprovider",
                    this)

    private fun getCropImages() {
        val storageDir = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val files = storageDir?.listFiles()
        if (files != null) {
            val filesCount = files.size
            imageCount.text = "${filesCount} crop images"
            if (filesCount > 0) {
                Picasso.get().load(files[filesCount-1]).into(imageView6)
            }
            if (files.size > 1) {
                Picasso.get().load(files[filesCount-2]).into(imageView7)
            }
        }

    }

    private fun openGallery() {
        Navigation.findNavController(view!!).navigate(R.id.action_homeFragment_to_cropGalleryFragment)
    }
}
